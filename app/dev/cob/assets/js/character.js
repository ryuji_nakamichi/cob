$(window).on('load', function(){
})

$(window).on("scroll resize", function() {
    new ScrollClassAdd($('._an'));

    for (var i = 0; i < 7; i++) {
        var cnt = i + 1;
        characterListAnimation('.character-list_0' + cnt);
    }

})


function characterListAnimation (_selecter) {
    if ( $(_selecter +' .character-list__item:first-child').hasClass('_o') ) {
        $(_selecter + ' .character-list__item:first-child').addClass('leftRight');
        $(_selecter + ' .character-list__item:not(' + _selecter + ' .character-list__item:first-child)').addClass('characterList');
    }
}

function btnTop () {
    new Vue({
        el: '#c-btn_top',
        components: { MyComponent }
    })
}

function characterList () {

    var dataObj = new Vue({
        el: '#data',
        data: {
            categorys:[
                {categorys_name:'現世組', categorys_number:'01', categorys_animation:'',
                    caracter:[
                        {name: "黒崎一護", charaNameClass: "ichigo", description: "生まれながらにして高い霊能力素質を備え、<br>死神・完現術・虚・滅却師全ての能力を備えている。"},
                        {name: "井上織姫", charaNameClass: "orihime", description: "一護の仲間で能力者。<br>藍染によれば神の領域をも侵すとされる。"},
                        {name: "石田雨竜", charaNameClass: "uryu", description: "一護の仲間で滅却師。<br>ユーハバッハの後継者に選ばれる。"},
                        {name: "茶渡泰虎", charaNameClass: "sado", description: "一護の仲間で能力者。<br>腕に宿る能力で敵を粉砕する。"},
                        {name: "浦原喜助", charaNameClass: "urahara", description: "浦原商店店主。<br>その正体は元護廷十三隊十二番隊隊長兼初代技術開発局局長。"},
                        {name: "四楓院夜一", charaNameClass: "yoruichi", description: "四楓院家第24代当主。<br>白打と歩法の達人であり、元護廷十三隊二番隊隊長兼隠密機動総司令官。"},
                        {name: "黒崎一心", charaNameClass: "isshin", description: "一護の父親で裏原と旧知の間柄。<br>元護廷十三隊十番隊隊長で、日番谷と乱菊の上官だった。"},
                        {name: "黒崎真咲", charaNameClass: "masaki", description: "一護の母親で純血統滅却師の生き残り。<br>藍染の虚化の実験にて一心と裏原に救われる。"},
                        {name: "白一護", charaNameClass: "white_ichigo", description: "一護の内なる虚の人格。<br>一護が絶対絶命の時に度々出てきた。"}
                    ]
                },
                {categorys_name:'護廷十三隊', categorys_number:'02', categorys_animation:'',
                    caracter: [
                        {name: "山本元柳斎重國", charaNameClass: "genryusai", description: "護廷十三隊一番隊隊長兼総隊長。二千歳を超えているが<br>未だその強大な力は衰えておらず、藍染でさえも一目置くほどである。"},
                        {name: "砕蜂", charaNameClass: "soifon", description: "護廷十三隊二番隊隊長兼隠密機動総司令官。<br>始解による二撃決殺と白打を得意とする。"},
                        {name: "鳳橋楼十郎", charaNameClass: "otoribashi", description: "音楽を愛する伊達男。<br>藍染の反乱以後、三番隊隊長に復帰する。"},
                        {name: "卯ノ花列", charaNameClass: "unohana", description: "一千年前の初代十三隊からの古株の護廷十三隊四番隊隊長。<br>十一番隊の原型を作ったとされる。"},
                        {name: "平子真子", charaNameClass: "hirako", description: "藍染のことを百年前から警戒していた護廷十三隊五番隊隊長。<br>藍染の反乱以後五番隊隊長に復帰する。"},
                        {name: "朽木白哉", charaNameClass: "byakuya", description: "ルキアの義兄で護廷十三隊六番隊隊長。<br>非常にバランスの取れた能力をもつ。"},
                        {name: "狛村左陣", charaNameClass: "komamura", description: "護廷十三隊七番隊隊長。<br>元柳斎の恩義に報いるため戦う。"},
                        {name: "享楽春水", charaNameClass: "syunsui", description: "護廷十三隊八番隊隊長。<br>確かな実力と冷静な判断力を併せ持つ。"},
                        {name: "六車拳西", charaNameClass: "kensei", description: "百年前の策略によって虚化。<br>藍染の反乱後、九番隊隊長に復帰する。"},
                        {name: "日番谷冬獅郎", charaNameClass: "hitsugaya", description: "護廷十三隊最年少の十番隊隊長。<br>冷静だが内に秘める感情は熱い。"},
                        {name: "更木剣八", charaNameClass: "zaraki", description: "戦いにしか関心がない十一番隊隊長。<br>幼い頃に闘った八千流との再戦を望む。"},
                        {name: "浮竹十四郎", charaNameClass: "ukitake", description: "最も平和を愛する十三番隊隊長。<br>ルキアの上官である、死神代行の制度を作った。"},
                        {name: "虎徹勇音", charaNameClass: "isane", description: "卯ノ花を敬愛する四番隊副隊長。<br>回道を扱え、鬼道に優れている。"},
                        {name: "阿散井恋次", charaNameClass: "renji", description: "好戦的な六番隊副隊長。<br>白哉を尊敬し、ルキアの幼馴染である。"},
                        {name: "伊勢七緒", charaNameClass: "nanao", description: "知的な八番隊副隊長。<br>享楽の監視役でもある。"},
                        {name: "檜佐木修兵", charaNameClass: "hisagi", description: "面倒見の良い九番隊副隊長。<br>東仙と六車を尊敬している。"},
                        {name: "松本乱菊", charaNameClass: "rangiku", description: "大人な魅力の十番隊副隊長。<br>一心の隊長時代からの副隊長である。"},
                        {name: "朽木ルキア", charaNameClass: "rukia", description: "一護の世界を変えた恩人で十三番隊副隊長。<br>尸魂界編以降は共に戦う仲間となる。"},
                        {name: "矢胴丸リサ", charaNameClass: "risa", description: "虚化の実験により現世へと身を隠していた元八番隊副隊長。<br>滅却師との決戦後、八番隊隊長へとなる。"}
                    ]
                },
                {categorys_name:'虚圏組', categorys_number:'03', categorys_animation:'',
                    caracter: [
                        {name: "藍染惣右介", charaNameClass: "aizen", description: "元護廷十三隊五番隊隊長だがルキアの処刑を巡る動乱にて離反。<br>尸魂界から去り、破面の上に立つ。"},
                        {name: "市丸ギン", charaNameClass: "gin", description: "元護廷十三隊三番隊隊長だがルキアの処刑を巡る動乱にて離反。<br>尸魂界から去り、虚圏へ移る"},
                        {name: "コヨーテ・スターク", charaNameClass: "stark", description: "第1十刃の男。<br>藍染に従い空座町決戦に参戦する。"},
                        {name: "バラガン・ルイゼンバーン", charaNameClass: "baraggan", description: "配下からは大帝と称される第2十刃。<br>藍染に従い空座町決戦に参戦する。"},
                        {name: "ティア・ハリベル", charaNameClass: "harribel", description: "第3十刃であり、紅一点。<br>藍染に忠誠的である。"},
                        {name: "ウルキオラ・シファー", charaNameClass: "ulquiorra", description: "第4十刃の男。<br>冷静な判断力を持ち、藍染への忠義心が高い。"},
                        {name: "ノイトラ・ジルガ", charaNameClass: "nnoitra", description: "第5十刃の男。<br>戦いが生きる全てだと考えている。"},
                        {name: "グリムジョー・ジャガージャック", charaNameClass: "grimmjow", description: "第6十刃の男。<br>一護とは度重なる因縁がある。"},
                        {name: "ゾマリ・ルルー", charaNameClass: "zommari", description: "第7十刃の男。<br>一護とは度重なる因縁がある。"},
                        {name: "ザエルアポロ・グランツ", charaNameClass: "szayelaporro", description: "第8十刃の男。<br>虚圏の科学者でもある。"},
                        {name: "アーロニーロ・アルルエリ", charaNameClass: "aaroniero", description: "十刃唯一の下級大虚の第9十刃。<br>無限に進化し続けることが可能。"},
                        {name: "ヤミー・リヤルゴ", charaNameClass: "yammy", description: "破面No.10の大男。<br>刀剣解放することで、第0十刃へと変わる。"},
                        {name: "ネリエル・トゥ・オーデルシュヴァンク", charaNameClass: "nelliel", description: "一護たちと行動を共にする破面の一人。<br>正体は元第3十刃。"},
                        {name: "ホワイト", charaNameClass: "white", description: "藍染により、死神の魂魄を元にして作られた虚。<br>虚化の実験にて現世に現れる。"}
                    ]
                },
                {categorys_name:'XCUTION', categorys_number:'04', categorys_animation:'',
                    caracter: [
                        {name: "銀城空吾", charaNameClass: "ginjyo", description: "完現術者の男でありXCUTIONのリーダーであるが、<br>その正体は初代死神代行。"},
                        {name: "月島秀九朗", charaNameClass: "tsukishima", description: "完現術者で、銀城の仲間。<br>しかし、ある目的を果たすために銀城と敵対を演じていた。"},
                        {name: "毒ヶ峰リルカ", charaNameClass: "riruka", description: "完現術者で、銀城の仲間。<br>許可した対象を自身の領域内に入り込ませる能力を持つ。"}
                    ]
                },
                {categorys_name:'『見えざる帝国（ヴァンデンライヒ）』', categorys_number:'05', categorys_animation:'',
                    caracter: [
                        {name: "ユーハバッハ", charaNameClass: "yhbh", description: "滅却師の始祖であり、全ての滅却師の王。<br>年の刻の中で力を蓄え、尸魂界侵略の機を窺う。"},
                        {name: "ユーグラム・ハッシュヴァルト", charaNameClass: "haschwalth", description: "星十字騎士団最高であり、「支配者の仮面」を与えられた男。<br>天鎖斬月を両断できるほどの力を有する。"},
                        {name: "ペルニダ・パルンカジャス", charaNameClass: "pernida", description: "星十字騎士団で親衛隊の一人。<br>霊王の左腕との噂がある。"},
                        {name: "アスキン・ナックルヴァール", charaNameClass: "nakklevaar", description: "親衛隊の一人でDの聖文字を持つ。<br>耐久力が異常に高い。"},
                        {name: "バンビエッタ・バスターバイン", charaNameClass: "bambietta", description: "星十字騎士団の一人でEの聖文字を持つ。<br>「爆撃」の力で敵を薙ぎ払う。"},
                        {name: "エス・ノト", charaNameClass: "asnodt", description: "星十字騎士団の一人でFの聖文字を持つ。<br>「恐怖」の力で敵を苦しめる。"},
                        {name: "リルトット・ランパード", charaNameClass: "liltotto", description: "Gの聖文字を持つ小柄な女性滅却師。<br>能力故か何か食べていることが多い。"},
                        {name: "バズビー", charaNameClass: "bazz-b", description: "星十字騎士団の一人でHの聖文字を持つ。<br>ハッシュヴァルトとは幼馴染。"},
                        {name: "蒼都", charaNameClass: "cangdu", description: "星十字騎士団の一人でIの聖文字を持つ。<br>足技が主体である。"},
                        {name: "キルゲ・オピー", charaNameClass: "quilge", description: "星十字騎士団の一人で、虚圏狩猟部隊統括狩猟隊長。<br>Jの聖文字の能力の監獄で敵を閉じ込める。"},
                        {name: "BG9", charaNameClass: "bg9", description: "星十字騎士団唯一の機械。<br>余裕のある態度で敵と対峙する。"},
                        {name: "ジェラルド・ヴァルキリー", charaNameClass: "gerard", description: "ユーハバッハの親衛隊の一人で星十字騎士団。<br>霊王の心臓と噂される。"},
                        {name: "ロバート・アキュトロン", charaNameClass: "accutrone", description: "星十字騎士団の中で古株の老紳士。<br>確かな戦闘力と分析力を誇る。"},
                        {name: "ドリスコール・ベルチ", charaNameClass: "driscoll", description: "星十字騎士団の一人でOの聖文字を持つ。<br>暴力的な性格。"},
                        {name: "ミニーニャ・マカロン", charaNameClass: "meninas", description: "星十字騎士団の一人でPの聖文字を持つ。<br>圧倒的パワーで粉砕する。"},
                        {name: "マスクド・マスキュリン", charaNameClass: "masculine", description: "Sの聖文字を持つ、自称正義の味方の星十字騎士団。<br>声援を浴びることで能力を上げる。"},
                        {name: "キャンディス・キャットニップ", charaNameClass: "candice", description: "星十字騎士団の女性滅却師。<br>能力で肉体そのものを稲妻に変えることができる。"},
                        {name: "グレミィ・トゥミュー", charaNameClass: "gremmy", description: "星十字騎士団最強を自負する滅却師。<br>イメージをそのまま反映することができる。"},
                        {name: "ニャンゾル・ワイゾル", charaNameClass: "nianzol", description: "星十字騎士団の一人でWの聖文字を持つ。<br>ユーハバッハと霊王宮へと侵攻する。"},
                        {name: "ジゼル・ジュエル", charaNameClass: "giselle", description: "星十字騎士団の一人でZの聖文字を持つ。<br>意外と耐久力が高い。"}
                    ]
                },
                {categorys_name:'零番隊', categorys_number:'06', categorys_animation:'',
                    caracter: [
                        {name: "兵主部一兵衛", charaNameClass: "hyosube", description: "尸魂界のすべての事象の名付け親で異名は「真名呼和尚」。<br>零番隊のリーダー格でもある。"},
                        {name: "麒麟寺天示郎", charaNameClass: "tenjiro", description: "零番隊の一角であり、「雷迅」の異名を持つ。<br>治癒の鬼道である「回道」を卯ノ花に教えた。"},
                        {name: "二枚屋王悦", charaNameClass: "oetsu", description: "零番隊の一角であり、「刀神」の異名を持つ。<br>全ての浅打は彼が精製している。"},
                        {name: "曳舟桐生", charaNameClass: "hikifune", description: "三百年くらい前から十二番隊長を務めていたが<br>尸魂界の歴史そのものと認められ王族特務零番隊に昇進した。"},
                        {name: "修多羅千手丸", charaNameClass: "senjyumaru", description: "零番隊の一角であり、「大織守」の異名を持つ。<br>技術開発局と関わりがあるらしい。"}
                    ]
                },
                {categorys_name:'その他', categorys_number:'07', categorys_animation:'',
                    caracter: [
                        {name: "志波空鶴", charaNameClass: "kukaku", description: "尸魂界の流魂街に拠点を構える花火師。<br>一護たちの瀞霊廷侵入を手助けした。"},
                        {name: "猿柿ひより", charaNameClass: "hiyori", description: "藍染の虚化の実験の犠牲になった元十二番隊副隊長。<br>事件後現世へと身を隠す。"}
                    ]
                }
            ]
        }
    })

}


$(function() {
    characterList();
    //_slickSlider();
});


/********************************************************************
スライダー処理
********************************************************************/
function _slickSlider () {

    $('.character-list').slick({
      infinite: true,
      dots: true,
      autoplay: false,
      pauseOnFocus: false,
      pauseOnHover: false,
      fade: false,
      autoplaySpeed: 2000,
      speed: 2000,
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      responsive: [

        {
            breakpoint: 767,
            settings: {
                infinite: true,
                dots: true,
                autoplay: false,
                pauseOnFocus: false,
                pauseOnHover: false,
                fade: false,
                autoplaySpeed: 3000,
                speed: 2000,
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
 });

}