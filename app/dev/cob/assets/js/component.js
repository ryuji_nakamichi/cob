//空のObject定義
var componentObj = {};


/*!* ---------------------------------------------------------------------------------------------
    星のアイコンSVG
--------------------------------------------------------------------------------------------- *!*/
componentObj.star = function starComponent () {
    var StarIcon = Vue.extend({
    props: {
        size: {
            type: Number,
            default: 20
        }
    },
    template:
        '<svg :width="size" :height="size" viewBox="0 0 16 16">' +
        '<path fill="#FBC02D" d="M16 6.2l-5.3-1.12-2.7-4.69v13.01l4.94 2.21-.57-5.39 3.63-4.02zm0 0"/>' +
        '<path fill="#FDD835" d="M5.3 5.09l-5.3 1.11 3.63 4.02-.57 5.39 4.94-2.21v-13.01l-2.7 4.7zm0 0"/>' +
        '</svg>'
    });
    var componentRoot = new Vue({
        template: '<div>' +
            '<star-icon></star-icon>' +
            'hello!' +
            '<star-icon :size="40"></star-icon>' +
            '<star-icon :size="60"></star-icon>' +
            '</div>',
        components: {
            StarIcon: StarIcon
        }
    });
    componentRoot.$mount('#MyAppRoot');
}


/*!* ---------------------------------------------------------------------------------------------
    トップページに戻るボタン
--------------------------------------------------------------------------------------------- *!*/
componentObj.backToTop = function backToTopComponent () {
    var componentRoot = new Vue({
        data: {
            button_text:'Home'
        },
        template: '<a href="/cob/" class="c-btn_top">' +
            '{{ button_text }}' +
            '</a>'
    });

    var _url = location.pathname;
    var _urlArray = _url.split('/');

    if (_urlArray[2] && !_urlArray[3]) {
        componentRoot.$mount('.backToTopRoot');
    }

}


/*!* ---------------------------------------------------------------------------------------------
    CHARACTERページに戻るボタン
--------------------------------------------------------------------------------------------- *!*/
componentObj.backToCharacterList = function backToCharacterListComponent () {
    var componentRoot = new Vue({
        data: {
            button_text:'To CHARACTER'
        },
        template: '<a href="/cob/character/" class="c-btn_detail character-list-back">' +
            '{{ button_text }}' +
            '</a>'
    });

    componentRoot.$mount('.backToCharacterListRoot');

}