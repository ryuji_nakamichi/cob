$(window).on('load',function(){
    new ScrollClassAdd($('.c-tit-lv_2'));
    new SpNav($('.menu-trigger'));
    pageTop($('.btn-pageTop'));
    toPageTop(500);
    bgm();
})

$(window).on("scroll resize", function() {
    new ScrollClassAdd($('.c-tit-lv_2'));
})

$(function() {
    componentObj.backToTop();
});

function ScrollClassAdd (selector) {

    var getScrollTopObj = new GetScrollTop();

    this.windowHeight   = $(window).height();
    this.bottomPos      = getScrollTopObj.bottomPos;
    this.selectorPos;
    this.triggerPoint;
    this.target;
    this.addClassName;
    this.selector = selector;


    //画面の下から4分の1に要素が入ってきたら
    for (var i = 0; i < this.selector.length; i++) {

        this.target = this.selector.eq(i);
        this.selectorPos  = this.target.offset().top;
        this.triggerPoint = this.selectorPos + Math.floor( (this.windowHeight / 4) );

        if (this.bottomPos > this.triggerPoint) {
            this.addClassName = checkType(this.target);
            this.target.addClass(this.addClassName);
            this.target.addClass('_o');
        }

    }
    

}

function SpNav (target) {

    this.target = target;

    this.target.on('click',function(){
      if($(this).hasClass('active')){
        $(this).removeClass('active');
        $('.sp-g-nav').removeClass('open');
        $('.overlay').removeClass('open');
      } else {
        $(this).addClass('active');
        $('.sp-g-nav').addClass('open');
        $('.overlay').addClass('open');
      }
    });
    $('.overlay').on('click',function(){
      if($(this).hasClass('open')){
        $(this).removeClass('open');
        this.target.removeClass('active');
        $('.sp-g-nav').removeClass('open');      
      }
    });

}


function pageTop (_selecter) {

    var selecter = _selecter;

    $(window).scroll(function () {

        if ( $(this).scrollTop() < 100 ) {
            selecter.removeClass('visible');
        } else {
            selecter.addClass('visible');
        }

    });

}

function toPageTop (speed) {
    $('.btn-pageTop').on('click', function() {
        $('body, html').animate({ scrollTop: 0 }, speed);
        return false;
    });
    
}

function checkType (selector) {

    if (selector.hasClass('_is-ani')) return 'is-animated';
    else if (selector.hasClass('_fi')) return 'fadeIn';
    else if (selector.hasClass('_fib')) return 'fadeInBounce';
    else if (selector.hasClass('_fibs')) return 'fadeInBounceSmall';
    else if (selector.hasClass('_fibm')) return 'fadeInBounceMedium';
    else if (selector.hasClass('_sm')) return 'smallMedium';
    else if (selector.hasClass('_ud')) return 'upDown';
    else if (selector.hasClass('_du')) return 'downUp';
    else if (selector.hasClass('_lr')) return 'leftRight';
    else if (selector.hasClass('_rl')) return 'rightLeft';
    else if (selector.hasClass('_ul')) return 'underline';
    else if (selector.hasClass('_wM')) return 'widthMax';
    else if (selector.hasClass('c-tit-lv_2')) return 'widthMax2';
    else if (selector.hasClass('_nFhM')) return 'nFheightMax';
    else if (selector.hasClass('_hM')) return 'heightMax';

}


/********************************************************************
ウィンドウ最下部のスクロール位置の取得処理
********************************************************************/
function GetScrollTop(){
    this.bottomPos = $(window).scrollTop() + $(window).height();
    return this.bottomPos;
}


/********************************************************************
指定した関数をtime秒後に実行する処理
********************************************************************/
function async(f, time) {
    setTimeout(f, time);
}


/********************************************************************
bgm処理
********************************************************************/
function bgm () {

    var audioBtn = $('.audio_button'),
      audioWrap = $('.audio_wrap'),
      audio = document.getElementById('audio');

      audioBtn.on('click', function () {
        if( audioWrap.hasClass('play') ) {
          audio.pause();
          audioWrap.removeClass('play');
        } else {
          audio.play();
          audioWrap.addClass('play');
        }
      });

}


function bgmVolueUpDown () {
    
}