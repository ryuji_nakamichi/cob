$(window).on('load',function(){
    new ScrollClassAdd($('._an'));
    mainVisualAniStart();
    mAnimStart($('.main-contents-list__item.story'));
})

$(window).on("scroll resize", function() {
    new ScrollClassAdd($('._an'));
    mAnimStart($('.main-contents-list__item.story'));
})


function mainContentsAnimetion () {
    var d1 = new $.Deferred();

    async(function() {
        mAnim_01();
        d1.resolve();
    }, 500);

    d1.promise()
    .then(function() {
        var d2 = new $.Deferred();

        async(function() {
            mAnim_02();
            d2.resolve();
        }, 500);

        return d2.promise();
    })
    .then(function() {
        var d3 = new $.Deferred();

        async(function() {
            mAnim_03();
            d3.resolve();
        }, 500);

        return d3.promise();
    });
}


function mAnim_01 (){
    return $(".main-contents-list__item.character").addClass('_an rightLeft');
}
function mAnim_02 (){
    return $(".main-contents-list__item.consideration").addClass('_an leftRight');
}
function mAnim_03 (){
    return $(".bg-band_main-contents").addClass('_an nFheightMax');
}

function mAnimStart (selecter) {

    if ( (selecter).hasClass('_o') ) {
        mainContentsAnimetion();
    }

}

function mainVisualAniStart () {
    var d1 = new $.Deferred();

    async(function() {
        mainVisualAni_01();
        d1.resolve();
    }, 700);
    d1.promise()
    .then(function() {
        var d2 = new $.Deferred();

        async(function() {
            mainVisualAni_02();
            d2.resolve();
        }, 700);

        return d2.promise();
    })
}

function mainVisualAni_01 () {
    $(".main-visual-txt").addClass('_an widthMax');
}
function mainVisualAni_02 () {
    $(".main-visual-txt__txt").addClass('_an fadeIn');
}