$(function() {

    componentObj.backToCharacterList();
    

    //グローバル変数宣言
    top_text            = $('.character-detail-nickname_top').text();
    bottom_text         = $('.character-detail-nickname_bottom').text();
    character_name_text = $('.character-detail-name__name').text();

    //テキストを一旦空にする
    $('.character-detail-nickname_top').text('');
    $('.character-detail-nickname_bottom').text('');
    $('.character-detail-name__name').text('');

    delay = 100 //1文字が表示される時間

    count_top_text_time            = textCount(top_text) + 0;
    count_bottom_text_time         = textCount(bottom_text) + 0;
    count_character_name_text_time = textCount(character_name_text) + 0;

});


$(window).on('load',function(){
    characterTextDisplay(top_text, bottom_text, character_name_text);
})

$(window).on("scroll resize", function() {
})


function textCount (_str = '') {

    var text_count = _str.length;
    var count_time = text_count * delay;

    return count_time;

}


function typing(selecter, str = "") {

    var buf    = selecter.text(); //書き込み済みの文字を要素から取得
    var writed = buf.length; //書き込み済みの文字数を取得
    var write  = "";

    if (writed < str.length){
        write = str.charAt(writed); //1文字だけ取得する
        selecter.text(buf + write); //1文字だけ追加していく
    } else {
        //buf = ""; //何度も繰り返すために内容を消去
    }

}

function shineAnimetion (selecter) {
    selecter.addClass('shineAni');
}
function fadeAnimetion (selecter) {
    selecter.addClass('_an fadeIn');
}


function characterTextDisplay (_str1 = '', _str2 = '', _str3 = '') {
    var d1 = new $.Deferred();

    async(function() {
        typing_01(_str1, delay);
        d1.resolve();
    }, 0);

    d1.promise()
    .then(function() {
        var d2 = new $.Deferred();

        async(function() {
            typing_02(_str2, delay);
            d2.resolve();
        }, count_top_text_time);

        return d2.promise();
    })
    .then(function() {
        var d3 = new $.Deferred();

        async(function() {
            typing_03(_str3, delay);
            d3.resolve();
        }, count_bottom_text_time);

        return d3.promise();
    })
    .then(function() {
        var d4 = new $.Deferred();

        async(function() {
            characterNameShine(delay);
            d4.resolve();
        }, count_character_name_text_time);

        return d4.promise();
    })
    .then(function() {
        var d5 = new $.Deferred();

        async(function() {
            characterImgFade(delay);
            d5.resolve();
        }, 0);

        return d5.promise();
    });
}

function typing_01 (_str, delay_time){
    return setInterval(function(){typing($('.character-detail-nickname_top'), _str);}, delay_time);
}
function typing_02 (_str, delay_time){
    return setInterval(function(){typing($('.character-detail-nickname_bottom'), _str);}, delay_time);
}
function typing_03 (_str, delay_time){
    return setInterval(function(){typing($('.character-detail-name__name'), _str);}, delay_time);
}
function characterNameShine (delay_time){
    return setInterval(function(){ shineAnimetion($('.character-detail-name-brock'));}, delay_time);
}
function characterImgFade (delay_time){
    return setInterval(function(){ fadeAnimetion($('.character-profile-img__inner'));}, delay_time);
}