$(function() {

    //グローバル変数宣言
    top_text = $('.detail-tit-tit').text();


    //テキストを一旦空にする
    $('.detail-tit-tit').text('');

    delay = 100 //1文字が表示される時間

    count_top_text_time    = textCount(top_text) + 400;

});


$(window).on('load',function(){
    considerationTextDisplay(top_text);
})

$(window).on("scroll resize", function() {
})


function textCount (_str = '') {

    var text_count = _str.length;
    var count_time = text_count * delay;

    return count_time;

}


function typing(selecter, str = "") {

    var buf    = selecter.text(); //書き込み済みの文字を要素から取得
    var writed = buf.length; //書き込み済みの文字数を取得
    var write  = "";

    if (writed < str.length){
        write = str.charAt(writed); //1文字だけ取得する
        selecter.text(buf + write); //1文字だけ追加していく
    } else {
        //buf = ""; //何度も繰り返すために内容を消去
    }

}


function considerationTextDisplay (_str1 = '') {
    var d1 = new $.Deferred();

    async(function() {
        typing_01(_str1, delay);
        d1.resolve();
    }, 0);

    d1.promise();
}

function typing_01 (_str, delay_time){
    return setInterval(function(){typing($('.detail-tit-tit'), _str);}, delay_time);
}